<?php
    //buat class
	class DBKoneksi{
		    //deklarasikan variabel yg dibutuhkan

		private $dbhost = "localhost";
		private $dbuser = "siswa";
		private $dbpass = "123456";
		private $dbname = "dbkegiatan";
	

	    //tambahan untuk PDO
    	private $opsi = [
    		PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    		PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    	];

    //buat koneksinya
    	private $koneksi = null;

    //buat fungsi untuk mendapat koneksi
    	public function __construct(){
    		$dsn = "mysql:host=".$this->dbhost.";dbname=".$this->dbname;
    		try{
    			$this->koneksi = new PDO ($dsn,
    									  $this->dbuser,$this->dbpass,
    									  $this->opsi);

    			// echo 'Koneksi Database SUKSEK!!'
    		}catch (PDOExeption $e){
    			echo $e->getMessage();
    		}
    	}
    	public function getKoneksi (){
    		return $this->koneksi;
    	}
    }
?>