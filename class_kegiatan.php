<?php
    //panggil file yang dibutuhkan	
	require_once "class_dbkoneksi.php";

	class kegiatan{
		private $tableName = "kegiatan";
		private $koneksi = null;

		public function __construct()
		{
			$database = new DBKoneksi ();
			$this->koneksi = $database->getKoneksi();
		}

		public function getAll(){
			$sql = "SELECT * FROM " . $this->tableName;
			$ps = $this->koneksi->prepare($sql);
			$ps->execute();
			return $ps->fetchAll();
		}
	}
    //deklarasi variabel
    //buat constructor untuk mendapat koneksi dgn db
    //buat fungsi untuk menampilkan semua data, insert, update, delete, find by id
?>