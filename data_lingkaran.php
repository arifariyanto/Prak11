<?php
    //panggil file sebelumnya agar fungsinya bisa digunakan
	require_once "class_lingkaran.php";

		echo " NILAI PHI ". Lingkaran::PHI;

    //ambil variabel dari class sebelumnya
		$lingkar1 = new Lingkaran (10);
		$lingkar2 = new Lingkaran (4);
    
    //buat variabel baru menggunakan fungsi di class sebelumnya
		echo "<br/> Luas Lingkaran 1 = " .$lingkar1->getLuas();
		echo "<br> Luas Lingkaran 2 = " .$lingkar2->getLuas();
    
    //gunakan fungsi class sebelumnya untuk mendapatkan hasil
		echo "<br>Keliling Lingkaran 1 = " .$lingkar1->getKeliling();
		echo "<br>Keliling Lingkaran 2 = " .$lingkar2->getKeliling();

?>